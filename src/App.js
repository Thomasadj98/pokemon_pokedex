import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";

//components
import Nav from "./components/Nav";
import PokemonDetails from "./components/PokemonDetails";
import PokemonList from "./components/PokemonList";

function App() {
  return (
    <Router>
      <div className="App">
        <Nav />
        <div className="container">
          <Switch>
            <Route path="/" exact component={PokemonList} />
            <Route path="/details/:name" component={PokemonDetails} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
