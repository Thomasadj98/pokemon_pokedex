import React from "react";

function Nav() {
  return (
    <div className="nav-container">
      <img src="assets/pokeball_icon.png" alt="pokébowl logo" width="60px" />
      <h1>Pokébowl</h1>
    </div>
  );
}

export default Nav;
