import React, { useState, useEffect } from "react";
import useFetch from "../custom_hooks/useFetch";
import PokemonCard from "./PokemonCard";
import "./Pokemon.css";

function PokemonList() {
  const [pokemons, setPokemons] = useState([]);
  const [limit, setLimit] = useState(20);
  const [offset, setOffset] = useState(0);

  const { get } = useFetch("https://pokeapi.co/api/v2/pokemon/");

  useEffect(() => {
    get(`?limit=${limit}&offset=${offset}`)
      .then((data) => {
        if (data) {
          setPokemons(data.results);
        }
      })
      .catch((error) => console.log(error));
  }, [get, limit, offset]);

  function handleNextCLick() {
    setOffset((prevValue) => prevValue + 20);
  }

  function handlePreviousCLick() {
    setOffset((prevValue) => prevValue - 20);
  }

  if (!pokemons) {
    return null;
  }

  return (
    <>
      <ul className="pokedex-grid">
        {pokemons &&
          pokemons.map((pokemon) => (
            <PokemonCard key={pokemon.name} pokemonUrl={pokemon.url} />
          ))}
      </ul>

      {offset !== 0 && (
        <button onClick={handlePreviousCLick}>Previous page</button>
      )}

      <button onClick={handleNextCLick}>Next page</button>
    </>
  );
}

export default PokemonList;
