import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import useFetch from "../custom_hooks/useFetch";

import { yesCap } from "../utils";

function PokemonCard({ pokemonUrl }) {
  const [pokemon, setPokemon] = useState();

  const { get } = useFetch(pokemonUrl);

  useEffect(() => {
    get("")
      .then((data) => {
        if (data) {
          setPokemon(data);
        }
      })
      .catch((error) => console.log(error));
  }, [get]);

  if (!pokemon) {
    return null;
  }

  return (
    <Link to={`details/${pokemon.name}`} className="router-link">
      <li key={pokemon.name} className="grid-item pokemon-card">
        <div>
          <h3>{yesCap(pokemon.name)}</h3>

          {pokemon.types.map((type) => (
            <img
              key={type.slot}
              src={`assets/${type.type.name}.png`}
              alt={type.type.name}
              className="pokemon-type"
            />
          ))}
        </div>

        <img
          alt={pokemon.name}
          src={pokemon.sprites.front_default}
          className="pokemon-card-img"
        />
      </li>
    </Link>
  );
}

export default PokemonCard;
