import React, { useEffect, useState } from "react";

import useFetch from "../custom_hooks/useFetch";
import { yesCap } from "../utils";

function PokemonDetails({ match }) {
  const [pokemonDetails, setPokemonDetails] = useState();

  const { get } = useFetch("https://pokeapi.co/api/v2/pokemon/");

  useEffect(() => {
    get(match.params.name)
      .then((data) => {
        if (data) {
          console.log(data);
          setPokemonDetails(data);
        }
      })
      .catch((error) => console.log(error));
  }, [get, match.params.name]);

  if (!pokemonDetails) {
    return null;
  }

  return (
    <div>
      <img
        src={pokemonDetails.sprites.front_shiny}
        alt={pokemonDetails.name}
        className="pokemon-details-img"
      />
      <h1 className="pokemon-name-title">{yesCap(pokemonDetails.name)}</h1>
      {/*pokemonDetails.types.map((type) => (
        <img
          key={type.slot}
          src={`assets/${type.type.name}.png`}
          alt={type.type.name}
          className="pokemon-type"
        />
      ))*/}
    </div>
  );
}

export default PokemonDetails;
